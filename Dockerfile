FROM python:3.10
WORKDIR /code

COPY ./Notification/requirements.txt /code/requirements.txt

RUN pip install --no-cache-dir --upgrade -r /code/requirements.txt

COPY ./Notification .


CMD ["python", "main.py"]

