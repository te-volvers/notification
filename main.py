import asyncio
import aioredis

from pydantic import BaseModel


class Measurement(BaseModel):
    device_id: str
    measurement: str
    timestamp: str


async def process_event(event):
    stream, data = event
    data = {k.decode(): v.decode() for k, v in data.items()}
    measurement = Measurement(**data)
    measurement = measurement.measurement.replace("kwh", "")
    if int(measurement) > 50:
        print("ALERTA: La medida ha superado el umbral de 50")


async def consume_events(stream_name, group_name):
    redis = await aioredis.from_url("redis://redis:6379")
    await redis.xgroup_destroy(stream_name, group_name)
    await redis.xgroup_create(stream_name, group_name, "$")
    while True:
        event = await redis.xreadgroup(group_name, "consumer2", streams={f'{stream_name}': '>'}, 
                                       count=None, block=0, noack=False)
        if event:
            await process_event(event[0][1][0])


async def main():
    await consume_events("measurements", "notification_group")


if __name__ == "__main__":
    asyncio.run(main())
